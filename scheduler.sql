-- 
-- DBMS_SCHEDULER demo
-- Skip Holidays
-- Last weekday per month
-- 

-- prepare

create or replace procedure show_schedule (
   p_name  in varchar2,
   p_times in number
) is
   l_start_date  timestamp;
   l_next_date   timestamp;
   l_return_date timestamp;
begin
   l_start_date := systimestamp;
   l_return_date := l_start_date;
   for ctr in 1..p_times loop
      dbms_scheduler.evaluate_calendar_string(p_name, l_start_date, l_return_date, l_next_date);
      dbms_output.put_line('Next Run on: ' || to_char(l_next_date,'yyyy-mm-dd hh24:mi:ss'));
      l_return_date := l_next_date;
   end loop;
end;
/

create or replace procedure mytestproc as
begin
    null;
end mytestproc;
/

exec DBMS_SCHEDULER.DROP_SCHEDULE('holidays');
exec DBMS_SCHEDULER.DROP_SCHEDULE('daily_tasks');
exec DBMS_SCHEDULER.DROP_SCHEDULE('last_weekday_per_month');

set SERVEROUTPUT on;

-- schedule some holidays

begin
   dbms_scheduler.create_schedule(
      schedule_name   => 'holidays',
      start_date      => trunc(sysdate),
      repeat_interval => 'FREQ=YEARLY; BYDATE=0227,0228,0303,0304',
      end_date        => null
   );
end;
/

exec SHOW_SCHEDULE('holidays', 8);

-- Daily task, excluding holidays

begin
   dbms_scheduler.create_schedule(
      schedule_name   => 'daily_tasks',
      start_date      => to_date('2025-02-23 19:00:00', 'yyyy-mm-dd hh24:mi:ss'),
      repeat_interval => 'Freq=Daily; ByDay=Mon, Tue, Wed, Thu, Fri; ByHour=19; exclude=holidays',
      end_date        => null
   );
end;
/

exec SHOW_SCHEDULE('daily_tasks', 8);

-- Last weekday per month

begin
   dbms_scheduler.create_schedule(
      schedule_name   => 'last_weekday_per_month',
      start_date      => to_date('2025-02-23 19:00:00', 'yyyy-mm-dd hh24:mi:ss'),
      repeat_interval => 'FREQ=MONTHLY; BYDAY=MON,TUE,WED,THU,FRI; BYSETPOS=-1',
      end_date        => null
   );
end;
/

exec SHOW_SCHEDULE('last_weekday_per_month', 8);

-- Create a job

begin
   dbms_scheduler.create_job(
      job_name        => 'my_test_job',
      job_type        => 'STORED_PROCEDURE',
      job_action      => 'myTestProc',
      schedule_name   => 'last_weekday_per_month',
      auto_drop       => false,
      comments        => 'My test job',
      enabled         => true  
   );
end;
/

-- Not needed when enabled=true

begin
   dbms_scheduler.run_job(
      job_name            => 'my_test_job',
      use_current_session => false
   );
end;
/

select * from USER_SCHEDULER_JOBS;
select * from USER_SCHEDULER_JOB_LOG where JOB_NAME not like 'DR$%' order by LOG_DATE desc;

execute DBMS_SCHEDULER.DROP_JOB('my_test_job');

-- run as sys
exec DBMS_SCHEDULER.PURGE_LOG;


--- EOF ---