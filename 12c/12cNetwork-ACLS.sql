/*
   12c Network ACL's
*/

-- run as sys
begin
    dbms_network_acl_admin.append_host_ace(
    host => '*',
    ace => xs$ace_type(privilege_list => xs$name_list('connect','resolve', 'http'),
       principal_name => 'SCOTT',
       principal_type => xs_acl.ptype_db));
end;
/

select * from dba_network_acl_privileges;

select host, lower_port, upper_port, acl, aclid,
       dbms_network_acl_admin.check_privilege_aclid(aclid,  'SCOTT', 'http') allowed
from   dba_network_acls;

-- select DBMS_NETWORK_ACL_ADMIN.check_privilege('NETWORK_ACL_FFA754E8E1270E14E0536D38A8C07C33', 'DEV', 'http');
-- exec DBMS_NETWORK_ACL_ADMIN.drop_acl('NETWORK_ACL_FFA754E8E1270E14E0536D38A8C07C33');

-- run as user

set serveroutput on

-- check access
-- Put some files in the static directory of the local ORDS
select * from table( dbms_network_acl_utility.domains('workshop.local') );

-- read some bytes from the URL
declare
   l_http_request  utl_http.req;
   l_http_response utl_http.resp;
   l_text          varchar2(1024);
begin
   l_http_request := utl_http.begin_request('http://workshop:8080/Gepardenforelle.html');
   l_http_response := utl_http.get_response(l_http_request);
   utl_http.read_text(l_http_response, l_text, 1024);
   dbms_output.put_line(l_text);
   utl_http.end_response(l_http_response);
end;
/

-- check, if errors occur
select utl_http.get_detailed_sqlerrm from dual;

-- EOF ---