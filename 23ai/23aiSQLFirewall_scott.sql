####################
### SQL Firewall ###
####################
### scott        ###
####################

--- 1 ---

CREATE TABLE IF NOT EXISTS one (a NUMBER, b VARCHAR2(10));
TRUNCATE TABLE one;
INSERT INTO one values 
   (1,'Bruening'),
   (2,'Willems');
CREATE VIEW IF NOT EXISTS two as SELECT sysdate AS mydate;
GRANT SELECT  ANY TABLE ON SCHEMA scott TO app_user;
GRANT INSERT  ANY TABLE ON SCHEMA scott TO app_user;

--- App User 1 --->
