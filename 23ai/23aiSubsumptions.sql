-----------------------------------------
-- Subsumption of Views and Subqueries --
-----------------------------------------

SELECT *
FROM (SELECT avg(s.AMOUNT_SOLD) av
      FROM  sales s, products p
      WHERE s.prod_id = p.prod_id
      AND   s.channel_id = 4
      AND   s.promo_id = 351) v1
      ,
     (SELECT sum(p.PROD_LIST_PRICE) sm
      FROM  sales s, products p
      WHERE s.prod_id = p.prod_id
      AND   s.channel_id = 4
      AND   s.promo_id = 999) v2;


SELECT avg(CASE WHEN s.PROMO_ID = 351
         THEN s.AMOUNT_SOLD ELSE NULL END) av,
       sum(CASE WHEN s.PROMO_ID = 999
         THEN p.PROD_LIST_PRICE ELSE NULL END) sm
FROM  sales s, products p
WHERE s.prod_id = p.PROD_ID
AND   s.CHANNEL_ID = 4
AND   s.PROMO_ID IN (351,999);


