-------------------------
--- JSON Duality View ---
-------------------------

---------------------------
--- Simple Flex Example ---
---------------------------


CREATE TABLE flextable 
   (id_col NUMBER PRIMARY KEY,
    col1   VARCHAR2(10),
    col2   VARCHAR2(10),
    myflex JSON(OBJECT));
    
INSERT INTO flextable VALUES (1,'first','entry',null);
COMMIT;

select * from flextable;
 
--- create a simple Duality view which maps the JSON column as flex 
CREATE OR REPLACE JSON RELATIONAL DUALITY VIEW flexview AS
  flextable @insert @update @delete
    {_id  : id_col,
     field1 : col1,
     field2 : col2,
     myflex @flex};


--- check view & table (and copy the document)
SELECT json_serialize(data pretty) FROM flexview;

SELECT * FROM flexview;


--- update the view with no mapped attributes and have a look into that table
UPDATE flexview fv SET data = '{
  "_id" : 1,
  "_metadata" :
  {
    "etag" : "876292EC3C89EBB64A1A4CC60F2DB7C9",
    "asof" : "0000000000530F51"
  },
  "field1" : "second",
  "field2" : "entry",
  "shoesize"  : "42",
  "toes"      : "10"}'
WHERE fv.data."_id" = 1;

SELECT * FROM flextable;

--- rollback and just to remember JSON_TRANSFORM let's try this (where the etag is correct by definition)
ROLLBACK;
UPDATE flexview fv SET data = JSON_TRANSFORM (data,
                                                SET '$.field1' = 'second',
                                                SET '$.shoesize' = '42',
                                                SET '$.toes' = '10')
 WHERE fv.data."_id" = 1;

SELECT * FROM flextable;


---------------------------------------
--- FROM JSON to Relational & Back ---
---------------------------------------

--- create collection ann load some data
CREATE JSON COLLECTION TABLE po_collection;

INSERT INTO po_collection  values (
 json {
  '_id' : 16,
  'po_user' : {
      'user' : 'wthiem',
      'name' : 'Wolfgang Thiem',
      'street' : 'Riesstr. 25',
      'city' : 'Munich',
      'state' : 'BY',
      'zipCode' : 80992,
      'country' : 'Germany'
       },
  'po_products': [
        {
            'ItemNumber' : 1,
            'Description' : 'A Fish Called Wanda',
            'UnitPrice' : 9.95,
            'Code' : 1302300419,
            'Quantity' : 1
        },
        {
            'ItemNumber' : 2,
            'Description' : 'La Grande vadrouille',
            'UnitPrice' : 7.95,
            'Code' : 7077291004,
            'Quantity' : 3
        },
        {
           'ItemNumber' : 3,
           'Description' : 'Kill the Boss',
           'UnitPrice' : 9.95,
           'Code' : 4339621749,
           'Quantity' : 1
        },
        {
          'ItemNumber' : 4,
          'Description' : 'The Bourne Identity',
          'UnitPrice' : 14.95,
          'Code' : 4982536124,
          'Quantity' : 4
        },
        {
          'ItemNumber' : 5,
          'Description' : 'Love Actually',
          'UnitPrice' : 12.95,
          'Code' : 8463735127,
          'Quantity' : 1
         }
  ]
}
);

INSERT INTO po_collection  values (
 json {
  '_id' : 17,
  'po_user' : {
      'user' : 'abruening',
      'name' : 'Arne Bruening',
      'street' : 'Kühnehöfe 5 ',
      'city' : 'Hamburg',
      'state' : 'HH',
      'zipCode' : 22761,
      'country' : 'Germany'
  },
  'po_products': [
        {
            'ItemNumber' : 1,
            'Description' : 'La Grande vadrouille',
            'UnitPrice' : 7.95,
            'Code' : 7077291004,
            'Quantity' : 1
        },
        {
            'ItemNumber' : 2,
            'Description' : 'A Fish Called Wanda',
            'UnitPrice' : 9.95,
            'Code' : 1302300419,
            'Quantity' : 7
        },
        {
          'ItemNumber' : 3,
          'Description' : 'Love Actually',
          'UnitPrice' : 12.95,
          'Code' : 8463735127,
          'Quantity' : 3
         }
  ]
}
);
 

INSERT INTO po_collection  values (
 json {
  '_id' : 18,
  'po_user' : {
               'user' : 'rwillems',
               'name' : 'Rainer Willems',
               'street' : 'Neue Mainzer Str. 46-50',
               'city' : 'Frankfurt a.M.',
               'state' : 'HE',
               'zipCode' : 60311,
               'country' : 'Germany'
             },
  'po_products': [
        {
            'ItemNumber' : 4,
            'Description' : 'A Fish Called Wanda',
            'UnitPrice' : 9.95,
            'Code' : 1302300419,
            'Quantity' : 6
        },
        {
           'ItemNumber' : 3,
           'Description' : 'Kill the Boss',
           'UnitPrice' : 9.95,
           'Code' : 4339621749,
           'Quantity' : 1
        },
        {
          'ItemNumber' : 2,
          'Description' : 'The Bourne Identity',
          'UnitPrice' : 14.95,
          'Code' : 4982536124,
          'Quantity' : 2
        },
        {
          'ItemNumber' : 1,
          'Description' : 'Love Actually',
          'UnitPrice' : 12.95,
          'Code' : 8463735127,
          'Quantity' : 5
         }
  ]
}
);
 
INSERT INTO po_collection  values (
 json {
  '_id' : 19,
  'po_user' : {
      'user' : 'wthiem',
      'name' : 'Wolfgang Thiem',
      'street' : 'Riesstr. 25',
      'city' : 'Munich',
      'state' : 'BY',
      'zipCode' : 80992,
      'country' : 'Germany'
       },
  'po_products': [
        {
          'ItemNumber' : 1,
          'Description' : 'Love Actually',
          'UnitPrice' : 12.95,
          'Code' : 8463735127,
          'Quantity' : 4
         },
        {
            'ItemNumber' : 2,
            'Description' : 'La Grande vadrouille',
            'UnitPrice' : 7.95,
            'Code' : 7077291004,
            'Quantity' : 12
        },
        {
          'ItemNumber' : 3,
          'Description' : 'The Bourne Identity',
          'UnitPrice' : 14.95,
          'Code' : 4982536124,
          'Quantity' : 4
        }
  ]
}
);

--- take a look at the collection
SELECT JSON_SERIALIZE(data PRETTY) from po_collection;

DECLARE
  dg CLOB;
  BEGIN
    SELECT JSON_DATAGUIDE(data, DBMS_JSON.FORMAT_HIERARCHICAL,DBMS_JSON.PRETTY)
      INTO dg
      FROM po_collection;
    DBMS_JSON.CREATE_VIEW( viewname  => 'PO_VIEW',
                           tablename => 'PO_COLLECTION',
                           jcolname  => 'data',
                           dataguide => dg);
  END;
/

SELECT * FROM po_view;

--- See Definition of automatically generated View
SET LONG 100000
SELECT DBMS_METADATA.GET_DDL('VIEW', 'PO_VIEW');

--- move data to relational model

CREATE TABLE po_user AS
  SELECT DISTINCT "user" As user_id,
                  "name" as name,
                  "street" as street,
                  "city" as city,
                  "state" as state,
                  "zipCode" as plz,
                  "country" as country
          FROM    po_view;       
          
ALTER TABLE po_user ADD CONSTRAINT po_user_pk PRIMARY KEY (user_id);
        
          
CREATE TABLE po_products AS
  SELECT DISTINCT "Code" as code,
                  "Description" as description,
                  "UnitPrice" as unitprice
          FROM    po_view;       
          
ALTER TABLE po_products ADD CONSTRAINT po_products_pk PRIMARY KEY (code);

CREATE TABLE po_orders AS
  SELECT distinct "_id" as id,
                   "user" as user_id
           FROM    po_view;       
           
     
           
ALTER TABLE po_orders ADD CONSTRAINT po_orders_pk PRIMARY KEY (id);     
ALTER TABLE po_orders ADD CONSTRAINT po_orders_fk_u FOREIGN KEY (user_id) REFERENCES po_user;   

CREATE TABLE po_lineitems AS
   SELECT "_id" as ponumber,
          "ItemNumber" as item_no,
          "Code" as code,
          "Quantity" as quantity
     FROM po_view
    WHERE 1=2;   
     
ALTER TABLE po_lineitems ADD item_id NUMBER GENERATED BY DEFAULT AS IDENTITY;    
ALTER TABLE po_lineitems ADD CONSTRAINT po_lineitems_pk PRIMARY KEY (item_id); 
ALTER TABLE po_lineitems ADD CONSTRAINT po_lineitems_uk UNIQUE (ponumber, item_no);  
ALTER TABLE po_lineitems ADD CONSTRAINT po_lineitems_fk_p FOREIGN KEY (code) REFERENCES po_products;   
ALTER TABLE po_lineitems ADD CONSTRAINT po_lineitems_fk_o FOREIGN KEY (ponumber) REFERENCES po_orders; 

INSERT INTO po_lineitems (ponumber,item_no,code,quantity)
    SELECT "_id" as ponumber,
          "ItemNumber" as item_no,
          "Code" as code,
          "Quantity" as quantity
     FROM po_view; 


--- recreate View

CREATE OR REPLACE VIEW po_view AS
   SELECT u.user_id, u.name, u.street, u.plz, u.city, o.id as ponumber, i.item_no, i.quantity, p.code, p.description, p.unitprice
   FROM po_orders o, po_user u, po_products p, po_lineitems i
   WHERE o.user_id  = u.user_id  AND
         o.id = i.ponumber AND
         i.code     = p.code;
         
SELECT * FROM po_view;         

--- add flex columns

ALTER TABLE po_user add user_myflex JSON(OBJECT);
ALTER TABLE po_orders add order_myflex JSON(OBJECT);

---build Duality View

CREATE OR REPLACE JSON RELATIONAL DUALITY VIEW po_dv AS
po_orders @insert @update @delete
{
	_id   : id
    order_myflex @flex
	po_user @update 
    {  user   : user_id
       name   : name
       street : street
       city   : city
       state  : state
       zipCode: plz
       country: country
       user_myflex @flex}
    po_products : po_lineitems  @insert @update @delete
   {
      Item_ID    : item_id
      ItemNumber : item_no
      Quantity   : quantity
      po_products @update @unnest
      {
        Description : description
        UnitPrice   : unitprice
        Code        : code
      } 
    }
};



SELECT JSON_SERIALIZE(data PRETTY) from po_dv;

-- insert new Order and change attributes of the used user
INSERT INTO po_dv (data) values (
   json {
  '_id' : 20,
  'po_user' :
  {
    'user' : 'wthiem',
    'name' : 'Wolfgang Thiem',
    'street' : 'xxx-street-xxx',
    'city' : 'xxx-city-xxx',
    'state' : 'BY',
    'zipCode' : 88888,
    'country' : 'Germany'
  },
  'po_products' :
  [
    {
      'ItemNumber' : 1,
      'Quantity' : 99,
      'Description' : 'La Grande vadrouille',
      'UnitPrice' : 7.95,
      'Code' : 7077291004
    }
  ]
}    
);

-- take a look at tables & View

select JSON_SERIALIZE(data PRETTY) from po_dv;

-- update existing po 17
UPDATE po_dv p set data = json 
{
  '_id' : 17,
  '_metadata' :
  {
    'etag' : '42ECF106FF4FE6E4436304674635BC20',
    'asof' : '0000000000654DFC'
  },
  'po_user' :
  {
    'user' : 'abruening',
    'name' : 'Arne Bruening',
    'street' : 'Kühnehöfe 5 ',
    'city' : 'Hamburg',
    'state' : 'HH',
    'zipCode' : 22761,
    'country' : 'Germany'
  },
  'po_products' :
  [
    {
      'Item_ID' : 6,
      'ItemNumber' : 1,
      'Quantity' : 1,
      'Description' : 'La Grande vadrouille',
      'UnitPrice' : 7.95,
      'Code' : 7077291004
    },
    {
      'Item_ID' : 7,
      'ItemNumber' : 2,
      'Quantity' : 1,
      'Description' : 'A Fish Called Wanda',
      'UnitPrice' : 9.95,
      'Code' : 1302300419
    }
  ]
}
WHERE p.data."_id" = 17;


--- insert with priority, floor & dogs

INSERT INTO po_dv (data) values (
   json {
  '_id' : 21,
  'priority' : 'urgent',
  'po_user' :
  {
    'user' : 'wthiem',
    'name' : 'Wolfgang Thiem',
    'street' : 'xxx-street-xxx',
    'city' : 'xxx-city-xxx',
    'state' : 'BY',
    'floor' : 1,
    'zipCode' : 88888,
    'country' : 'Germany',
    'dogs' : ['Greyhound','Poodle','Dwarf Pinscher']
  },
  'po_products' :
  [
    {
      'ItemNumber' : 1,
      'Quantity' : 1,
      'Description' : 'La Grande vadrouille',
      'UnitPrice' : 7.95,
      'Code' : 7077291004
    }
  ]
}    
);

SELECT JSON_SERIALIZE(data PRETTY) from po_dv p WHERE p.data."_id" = 21;

-- take a look at Orders & User

SELECT user_id, name, floor, dog_breed
FROM po_user p, 
     JSON_TABLE(user_myflex, 
                '$' 
                COLUMNS ( floor NUMBER,
                          NESTED PATH '$.dogs[*]' COLUMNS (dog_breed VARCHAR2 PATH '$'))
               )           
;

