---------------------------------------
--- Create and configure User scott ---
---------------------------------------

PROMPT
PROMPT ###### setup-23-Demo-users.sql ######

-- run as SYS

DROP USER IF EXISTS app_user CASCADE;

DROP USER IF EXISTS scott cascade;

DROP ROLE IF EXISTS myrole;

CREATE USER scott identified by oracle;
GRANT DB_DEVELOPER_ROLE to scott;
ALTER USER scott QUOTA UNLIMITED ON "USERS";
GRANT CREATE ANY DIRECTORY to scott;
GRANT SELECT ANY TABLE ON SCHEMA hr TO scott;
GRANT SQL_FIREWALL_ADMIN to scott;
GRANT SQL_FIREWALL_VIEWER to scott;
GRANT CREATE PROPERTY GRAPH to scott;

grant select on V_$SQL_WORKAREA to scott;
grant select on V_$SQL_PLAN to scott;
grant select on V_$SQL_PLAN_STATISTICS to scott;
grant select on V_$SQL_PLAN_STATISTICS_ALL to scott;
grant select on V_$SQL_PLAN_MONITOR to scott;
GRANT SELECT ON V_$MYSTAT to scott;
GRANT SELECT ON V_$SQL to scott;
GRANT SELECT ON V_$SESSION to scott;
GRANT SELECT ON V_$SQL_PLAN_STATISTICS_ALL to scott;

--- EOF --------------------------------------