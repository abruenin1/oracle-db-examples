---------------------------------------
--- Setup for MLE JavaScript Demo   ---
---------------------------------------

PROMPT
PROMPT ###### 23cMLE_JavaScript-setup.sql ######

-- https://blogs.oracle.com/developers/post/how-to-import-javascript-es-modules-in-23c-free-and-use-them-in-sql-queries

-- grant scott create directory 
-- GRANT CREATE ANY DIRECTORY to scott;

-- create the directory to access filesystem where we'll download our ES Modules
-- CREATE DIRECTORY IF NOT EXISTS mle_dir AS '/home/oracle/mle';

GRANT EXECUTE ON JAVASCRIPT TO scott;

-- create directory mle under /home/oracle/ and go there

-- For the rest, we'll focus on the Chance.js module from Victor Quinn under MIT License. 
-- https://www.jsdelivr.com/package/npm/chance
-- This module is really interesting when working on data as it provides numerous ways to "generate random numbers, characters, strings, names, addresses, dice, 
-- and pretty much anything else"; in brief 100+ new data generators.
-- The latest version available at the time of writing is 1.1.11, so let's download it into our mle folder so that it can be used by the database

-- download Chance.js S Module 
-- curl -Lo ./mle/chance.js https://cdn.jsdelivr.net/npm/chance@1.1.11/+esm



