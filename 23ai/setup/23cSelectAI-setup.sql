PROMPT
PROMPT ###### 23cSelectAI-setup.sql ######

--https://docs.oracle.com/en/cloud/paas/autonomous-database/serverless/adbsb/sql-generation-ai-autonomous.html

-- admin

-- allow network access for users to URLSs (OpenAI & Cohere)
BEGIN  
    DBMS_NETWORK_ACL_ADMIN.APPEND_HOST_ACE(
         host => 'api.cohere.ai',
         ace  => xs$ace_type(privilege_list => xs$name_list('http'),
                             principal_name => 'ADMIN',
                             principal_type => xs_acl.ptype_db)
   );
END;
/

BEGIN  
    DBMS_NETWORK_ACL_ADMIN.APPEND_HOST_ACE(
         host => 'api.openai.com',
         ace  => xs$ace_type(privilege_list => xs$name_list('http'),
                             principal_name => 'ADMIN',
                             principal_type => xs_acl.ptype_db)
   );
END;
/


BEGIN  
    DBMS_NETWORK_ACL_ADMIN.APPEND_HOST_ACE(
         host => 'api.cohere.ai',
         ace  => xs$ace_type(privilege_list => xs$name_list('http'),
                             principal_name => 'SCOTT',
                             principal_type => xs_acl.ptype_db)
   );
END;
/

BEGIN  
    DBMS_NETWORK_ACL_ADMIN.APPEND_HOST_ACE(
         host => 'api.openai.com',
         ace  => xs$ace_type(privilege_list => xs$name_list('http'),
                             principal_name => 'SCOTT',
                             principal_type => xs_acl.ptype_db)
   );
END;
/

-- create credentials for external services
execute DBMS_CLOUD.create_credential('COHERE_CRED', 'COHERE', 'xxx');
execute DBMS_CLOUD.create_credential('OPENAI_CRED', 'OPENAPI', 'xxx');

GRANT EXECUTE ON DBMS_CLOUD_AI to scott;

/* 
SELECT * FROM all_credentials;

execute DBMS_CLOUD.drop_credential('COHERE_CRED');
execute DBMS_CLOUD.drop_credential('OPENAI_CRED');
*/

