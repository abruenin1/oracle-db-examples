----------------------------------
--- Ubiquitous Database Search ---
----------------------------------

PROMPT
PROMPT ###### 23cUbiquitousSearch-setup.sql ######

-- run as sys
-- GRANT SELECT ANY TABLE ON SCHEMA hr TO scott;
-- requires EMPLOYEES / DEPARTMENTS tables in HR schema
-- The required scripts to setup the HR-schema can be found on GitHub 
-- under https://github.com/oracle-samples/db-sample-schemas. 
-- For easy setup, you could use hr_quick_start.sql 
-- from https://github.com/connormcd/misc-scripts.

-- run as user
DROP TABLE IF EXISTS employees;
DROP TABLE IF EXISTS departments;

CREATE TABLE employees AS SELECT * FROM hr.employees;
CREATE TABLE departments AS SELECT * FROM hr.departments;





