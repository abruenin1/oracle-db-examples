----------------------------------------
--- Startpoint is a 23c Installation ---
----------------------------------------

--- run as SYS from sqlcl

SET ECHO OFF
CLEAR SCREEN

------------------------------
--- (RE-)CREATE DEMO USERS ---
------------------------------

@setup-23c-Demo-users.sql

-------------------
--- Prepare MLE ---
-------------------

@23cMLE_JavaScript-setup.sql

---------------------------
--- Setup as user scott ---
---------------------------

conn scott/oracle

----------------------------------------------------
--- run utlxplan.sql for showing execution plans ---
----------------------------------------------------

@setup-xplan.sql

--------------------------
--- Prepare Graph Data ---
--------------------------

@23cPropertyGraph-setup.sql

----------------------------------------------------
--- Prepare Table for Ubiquitous Database Search ---
----------------------------------------------------

@23cUbiquitousSearch-setup.sql

-------------------------
--- Prepare SELECT AI ---
-------------------------

/* done in ADB, not 23c !!!! */

-- @23cSelectAI-setup.sql