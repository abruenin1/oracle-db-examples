-----------------------------
--- Priority Transactions ---
-----------------------------
--- Session 2 - SYS ---
------------------------


ALTER SYSTEM SET priority_txns_high_wait_target = 5; 
ALTER SYSTEM SET priority_txns_medium_wait_target =10; 
ALTER SYSTEM SET priority_txns_mode = ROLLBACK;  -- TRACK

ALTER SESSION SET txn_priority = high;

UPDATE scott.emp SET sal = 400 WHERE ename = 'KING';


SELECT * FROM scott.emp;