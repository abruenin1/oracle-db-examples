--------------------------------
--- Automatic SQL Transpiler ---
--------------------------------

CREATE OR REPLACE FUNCTION datetostring_f (p_hiredate DATE) RETURN VARCHAR2 IS
BEGIN
   RETURN to_char(p_hiredate,'YYYY.MM.DD');
END;
/

SELECT count(*)
  WHERE datetostring_f ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;

SELECT * FROM dbms_xplan.display_cursor();

ALTER SESSION SET sql_transpiler = 'ON';

SELECT count(*)
  WHERE datetostring_f ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;

SELECT * FROM dbms_xplan.display_cursor();

ALTER SESSION SET sql_transpiler = 'OFF';


