--------------------------------------
--- UPDATE, DELETE -> Direct Joins ---
--------------------------------------

ALTER TABLE emp MODIFY job VARCHAR2(30);

UPDATE emp 
   SET job = job || ' (' ||loc|| ')'
  FROM dept
 WHERE emp.deptno = dept.deptno;
 -- potentially additional filters on dept table

SELECT * FROM emp;

DELETE emp 
  FROM dept
 WHERE emp.deptno = dept.deptno
   AND loc = 'DALLAS'; 
   
SELECT * FROM emp;

ROLLBACK; -- Dallas needed later


