####################
### SQL Firewall ###
####################

-- sys
CREATE USER app_user IDENTIFIED BY OraclePW987$$$ QUOTA UNLIMITED ON users;
GRANT CREATE SESSION TO app_user;


exec DBMS_SQL_FIREWALL.ENABLE;


SELECT * FROM dba_sql_firewall_status;



BEGIN
  dbms_sql_firewall.create_capture (
       username       => 'app_user',
       top_level_only => false,
       start_capture  => true);
END;
/


-- scott
CREATE TABLE IF NOT EXISTS one (a NUMBER, b VARCHAR2(10));
TRUNCATE TABLE one;
INSERT INTO one values 
   (1,'Bruening'),
   (2,'Willems');
COMMIT;
CREATE VIEW IF NOT EXISTS two as SELECT sysdate AS mydate;
GRANT SELECT  ANY TABLE ON SCHEMA scott TO app_user;
GRANT INSERT  ANY TABLE ON SCHEMA scott TO app_user;

-- app_user
SELECT count(*) FROM scott.one;
SELECT *        FROM scott.two;
INSERT INTO scott.one VALUES (3,'THIEM');
COMMIT;


-- sys
SELECT username, command_type, sql_text, accessed_objects, current_user, client_program, os_user 
  FROM dba_sql_firewall_capture_logs
 WHERE username = 'APP_USER';


exec dbms_sql_firewall.stop_capture('APP_USER');

select SQL_TEXT from DBA_SQL_FIREWALL_ALLOWED_SQL where username = 'APP_USER';

exec dbms_sql_firewall.generate_allow_list ('APP_USER');

SELECT * FROM dba_sql_firewall_allowed_ip_addr WHERE  username = 'APP_USER';
SELECT * FROM dba_sql_firewall_allowed_os_prog WHERE  username = 'APP_USER';
SELECT * FROM dba_sql_firewall_allowed_os_user WHERE  username = 'APP_USER';
SELECT * FROM dba_sql_firewall_allowed_sql     WHERE  username = 'APP_USER';

-- possible to change Allow List via 
-- dbms_sql_firewall.add_allowed_context
-- dbms_sql_firewall.delete_allowed_context

BEGIN
  dbms_sql_firewall.enable_allow_list (
    username => 'APP_USER',
    enforce  => dbms_sql_firewall.enforce_all,   
    block    => true);
END;
/

-- DBMS_SQL.ENFORCE_CONTEXT enforces the allowed contexts that have been configured.
-- DBMS_SQL.ENFORCE_SQL enforces the allowed SQL that has been configured.
-- DBMS_SQL.ENFORCE_ALL enforces both allowed contexts and allowed SQL. This setting is the default.



-- app_user
SELECT count(*) FROM scott.one;
SELECT count(*) FROM scott.two;
SELECT *        FROM scott.two;

-- sys
SELECT * FROM dba_sql_firewall_violations WHERE  username = 'APP_USER';

-- it's possible to add these "violations" to the white list
BEGIN
   dbms_sql_firewall.append_allow_list('APP_USER', dbms_sql_firewall.violation_log);
END;
/

-- app_user
SELECT count(*) FROM scott.two;

-- dbms_sql_firewall.flush_logs;
-- dbms_sql_firewall.purge_log /eg. time dependent
-- dbms_sql_firewall.exclude (dbms_sql_firewall.scheduler_job)

-- sys
SELECT view_name FROM   dba_views WHERE  view_name like 'DBA_SQL_FIREWALL%';

SELECT * FROM DBA_SQL_FIREWALL_CAPTURES;

DROP USER app_user;


