---------------------------
--- RENAME LOB Segments ---
---------------------------


CREATE TABLE mylob (id     NUMBER,
                    doc    BLOB);


SELECT table_name, column_name, segment_name FROM user_lobs WHERE table_name ='MYLOB';


ALTER TABLE mylob RENAME LOB(doc) SYS_LOB0000071640C00002$$ TO meaningfulname;


SELECT table_name, column_name, segment_name FROM user_lobs WHERE table_name ='MYLOB';

-- before you had to do a ALTER TABLE MOVE for that


