---------------------------
--- SQL ANALYSIS REPORT ---
---------------------------

SELECT d.*
FROM hr.departments d, hr.employees e 
WHERE d.department_id = e.department_id
      AND substr(phone_number,1,1) = '1'
   UNION 
SELECT d.*
FROM hr.departments d, hr.employees e, hr.jobs j
WHERE d.department_id = e.department_id
      AND substr(email,1,2) = 'GB';


SELECT * FROM table(DBMS_XPLAN.DISPLAY_CURSOR(FORMAT=>'typical'));

SELECT d.*
FROM hr.departments d, hr.employees e 
WHERE d.department_id = e.department_id
      AND substr(phone_number,1,1) = '1'
   UNION -- Leave this
SELECT d.*
FROM hr.departments d, hr.employees e, hr.jobs j
WHERE d.department_id = e.department_id
      AND e.job_id = j.job_id -- FIX
      AND email like 'GB%'; -- FIX
      
SELECT * FROM table(DBMS_XPLAN.DISPLAY_CURSOR(FORMAT=>'typical'));


