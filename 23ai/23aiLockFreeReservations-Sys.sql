------------------------------
--- Lock-Free Reservations ---
------------------------------
--- Session 2 - SYS ---
-----------------------

--- 1 ---
SELECT * FROM scott.cinema;
UPDATE scott.cinema SET capacity = capacity - 20 WHERE id = 1;
--- 1 ---

--- 2 ---
SELECT * FROM scott.cinema;
COMMIT;
SELECT * FROM scott.cinema; 
--- 2 ---


--- 3 ---
SELECT * FROM scott.SYS_RESERVJRNL_71388;
SELECT * FROM scott.cinema;
UPDATE scott.cinema SET capacity = capacity - 10 WHERE id = 1;
SELECT * FROM scott.cinema;
SELECT * FROM scott.SYS_RESERVJRNL_71388;
COMMIT;
SELECT * FROM scott.cinema;
--- 3 ---

--- 4 ---
SELECT * FROM scott.cinema;
UPDATE scott.cinema SET capacity = capacity - 10 WHERE id = 1;
--- 4 ---

