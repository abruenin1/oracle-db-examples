---------------------------------------------------------------------------------
--- Data Quality Operators in Oracle Database - Fuzzy match & Phonetic Encode ---
---------------------------------------------------------------------------------

CREATE TABLE mytab (id      NUMBER,
                    value1  VARCHAR2(20),
                    value2  VARCHAR2(20));
                    
INSERT INTO mytab VALUES
  (1, 'Rainer Willems', 'Rainer Willens'),
  (2, 'Christoph Blessing', 'Christof Blässing'),
  (3, 'Marlon Deus', 'Marlon Brando'),
  (4, 'Wolfgang Thiem', 'Wolfgang Team'),
  (5, 'Thomas Gorsitzke', 'Arne Brüning'),
  (6, 'Ute Müller','Ute Müller');
  
COMMIT;                    
                    
SELECT value1, value2,
       fuzzy_match(levenshtein, value1, value2) AS levenshtein,
       fuzzy_match(damerau_levenshtein, value1, value2) AS damerau_levenshtein,
       fuzzy_match(jaro_winkler, value1, value2) AS jaro_winkler,
       fuzzy_match(bigram, value1, value2) AS bigram,
       fuzzy_match(trigram, value1, value2) AS trigram,
       fuzzy_match(whole_word_match, value1, value2) AS whole_word_match,
       fuzzy_match(longest_common_substring, value1, value2) AS longest_common_substring
FROM   mytab;

--- UNSCALED, RELATE_TO_SHORTER & EDIT_TOLERANCE 

SELECT value1, value2,
       fuzzy_match(whole_word_match, value1, value2, EDIT_TOLERANCE 1) AS whole_word_match_1,
       fuzzy_match(whole_word_match, value1, value2, EDIT_TOLERANCE 25) AS whole_word_match_25,
       fuzzy_match(whole_word_match, value1, value2, EDIT_TOLERANCE 75) AS whole_word_match_75,
       fuzzy_match(whole_word_match, value1, value2, EDIT_TOLERANCE 100) AS whole_word_match_100,
       fuzzy_match(whole_word_match, value1, value2) AS whole_word_match
FROM   mytab;


