-----------------------
--- Default on Null ---
-----------------------

CREATE TABLE SHOP_BASKET(id          NUMBER GENERATED ALWAYS AS IDENTITY,
                         product     VARCHAR2(20) ,
                         weight      NUMBER       DEFAULT '0',
                         unity       VARCHAR2(10) DEFAULT ON NULL 'kg',
                         best_before DATE         DEFAULT ON NULL FOR INSERT ONLY sysdate+21,
                         color       VARCHAR2(10) DEFAULT ON NULL FOR INSERT AND UPDATE 'green');
                  
--desc shop_basket              
info shop_basket
                                  
INSERT INTO SHOP_BASKET (product, weight, unity, best_before, color) VALUES
    ('PEARS',1500,'gr',sysdate+90,'yellow');
    
INSERT INTO SHOP_BASKET (product, weight, unity, best_before, color) VALUES    
    ('APPLE',null,null,null,null);
    
INSERT INTO SHOP_BASKET (product) VALUES
    ('BROCCOLI');
    
SELECT * FROM SHOP_BASKET;    

-- DEFAULT 0, does not fire on UPDATE
UPDATE SHOP_BASKET 
   SET weight = null;
   
SELECT * FROM SHOP_BASKET;    

-- DEFAULT ON NULL 'kg', does not fire on UPDATE
UPDATE SHOP_BASKET 
   SET unity = null;
   
-- DEFAULT ON NULL FOR INSERT ONLY sysdate+21, same as above
UPDATE SHOP_BASKET 
   SET best_before = null;
   
-- DEFAULT ON NULL FOR INSERT AND UPDATE 'green'  
UPDATE SHOP_BASKET 
   SET color = null;   
   
SELECT * FROM SHOP_BASKET;


