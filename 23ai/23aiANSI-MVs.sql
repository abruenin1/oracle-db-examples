----------------------------------------
--- MATERIALIZED VIEWS ON ANSI JOINS ---
----------------------------------------

CREATE MATERIALIZED VIEW empdept_mv1 AS
    SELECT * 
    FROM emp
    NATURAL JOIN dept;

CREATE MATERIALIZED VIEW empdept_mv2 AS
    SELECT * 
    FROM emp
    JOIN dept USING ( deptno );

CREATE MATERIALIZED VIEW empdept_mv3 AS
    SELECT * 
    FROM emp e
    JOIN dept d ON e.deptno = d.deptno;


