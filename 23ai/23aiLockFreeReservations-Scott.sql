------------------------------
--- Lock-Free Reservations ---
------------------------------
--- Session 1 - SCOTT ---
-------------------------

CREATE TABLE cinema
   ( id         NUMBER  PRIMARY KEY,
     film       VARCHAR2(30),
     capacity   NUMBER  CHECK (capacity >= 0),
     room       NUMBER,
     starting   DATE);
     
INSERT INTO cinema VALUES
    (1,'A Fish Called Wanda',100,1,null),
    (2,'Kill the Boss',200,2,null),
    (3,'The Bourne Identity',300,3,null),
    (4,'Love Actually',200,2,null),
    (5,'La Grande vadrouille',300,3,null);
    
COMMIT;    
    
SELECT * FROM cinema;     

UPDATE cinema SET capacity = capacity - 20 WHERE id = 1;

SELECT * FROM cinema;   

--- 1 --->

COMMIT;
SELECT * FROM cinema;

--- 2 --->

SELECT * FROM cinema;


--- define column as reservable
ALTER TABLE cinema MODIFY capacity RESERVABLE;

SELECT TABLE_NAME, COLUMN_NAME, DATA_TYPE, RESERVABLE_COLUMN 
FROM user_tab_columns WHERE table_name = 'CINEMA';

--- what's not working now
UPDATE cinema SET capacity = capacity - 10 WHERE film = 'A Fish Called Wanda';
UPDATE cinema SET capacity = 40 WHERE id = 1;
UPDATE cinema SET room = 2 , capacity = capacity - 10 WHERE id = 1;

UPDATE cinema SET capacity = capacity - 20  WHERE id = 1;

SELECT * FROM cinema;   

--- find object ID for table to find ReserveJournal
SELECT object_id FROM user_objects Where object_name='CINEMA';

SELECT * FROM SYS_RESERVJRNL_88110;

--- 3 --->

SELECT * FROM cinema;
COMMIT;
SELECT * FROM cinema;


-- when constraint is checked
UPDATE cinema SET capacity = capacity - 12 WHERE id = 1;
UPDATE cinema SET capacity = capacity - 12 WHERE id = 1;
UPDATE cinema SET capacity = capacity - 12 WHERE id = 1;

--- 4 --->

COMMIT;
SELECT * FROM cinema;


-- more as one reservable column possible
ALTER TABLE cinema MODIFY room RESERVABLE;

UPDATE cinema SET capacity = capacity + 100, room = room + 1 
WHERE id = 1;

SELECT * FROM SYS_RESERVJRNL_85761;


ALTER TABLE IF EXISTS cinema MODIFY room NOT RESERVABLE;





