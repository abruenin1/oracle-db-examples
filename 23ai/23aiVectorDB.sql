------------------------------
--- Vector Distance Search ---
------------------------------


SELECT  vector_distance ( vector('[0,3]') , 
                          vector('[4,0]') ,
                          EUCLIDEAN ) distance;

-- EXECUTE DBMS_VECTOR.LOAD_ONNX_MODEL('DM_DUMP','all-MiniLM-L6-v2.onnx','doc_model')

SELECT model_name, mining_function, algorithm, algorithm_type, model_size/1024/1024 "Size [MB]"
  FROM user_mining_models
  ORDER BY MODEL_NAME;

SELECT model_name, attribute_name, attribute_type, data_type, vector_info
  FROM user_mining_model_attributes
  ORDER BY MODEL_NAME;



DROP TABLE IF EXISTS article;

CREATE TABLE article (id NUMBER, text VARCHAR2(4000), vec VECTOR);

CREATE OR REPLACE TRIGGER embedding 
BEFORE INSERT OR UPDATE ON article 
FOR EACH ROW
BEGIN
 SELECT to_vector(vector_embedding(doc_model USING :new.text AS data)) INTO :new.vec;
   /* :new.vec := dbms_vector.utl_to_embedding(
          data => :new.text
       , params => json(q'[{"provider": "database", "model": "DOC_MODEL"}]'));  */
END;
/

INSERT INTO article VALUES
    (1,'Baseball is very popular in the US',null),
    (2,'Trees are very important for our environment',null),
    (3,'The 2024 Olympic Games will take place in Paris',null),
    (4,'Dirk Nowitzki played for the Dalles Mavericks',null),
    (5,'The Minions are yellow',null),
    (6,'Germany won the basketball world championship',null),
    (7,'There are a lot of animals in the rain forrest',null),
    (8,'The atmosphere at the Betzenberg in Kaiserslautern is great',null),
    (9,'Biodiversity is greater on uncultivated land', null),
    (10,'Lothar Matthäus was footballer of the year in europe in 1990', null);
    

SELECT * FROM article;

SELECT id, text FROM article
ORDER BY vector_distance(vec, to_vector(vector_embedding(doc_model USING 'Nature' as data)), COSINE);

SELECT id, text FROM article
ORDER BY vector_distance(vec, to_vector(vector_embedding(doc_model USING 'Sports' as data)), COSINE);

SELECT id, text FROM article
ORDER BY vector_distance(vec, to_vector(vector_embedding(doc_model USING 'Basketball' as data)), COSINE);

SELECT id, text FROM article
ORDER BY vector_distance(vec, to_vector(vector_embedding(doc_model USING 'Celebrity' as data)), COSINE);








