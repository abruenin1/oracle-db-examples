#################
### SELECT AI ###
#################

exec dbms_cloud_ai.drop_profile('COHERE');
exec dbms_cloud_ai.drop_profile('COHERE_2');
exec dbms_cloud_ai.drop_profile('OPENAI');
DROP TABLE scott.leute;

-- create profiles (context)
BEGIN
  DBMS_CLOUD_AI.create_profile(
      'COHERE',
      '{"provider"       : "cohere",
        "credential_name": "COHERE_CRED",
        "object_list"    : [{"owner": "SCOTT", "name": "EMP"},
                            {"owner": "SCOTT", "name": "DEPT"}]
       }');
end;
/

BEGIN
  DBMS_CLOUD_AI.create_profile(
      'OPENAI',
      '{"provider"       : "openai",
        "credential_name": "OPENAI_CRED",
        "object_list"    : [{"owner": "SCOTT", "name": "EMP"},
                            {"owner": "SCOTT", "name": "DEPT"}]
       }');
end;
/


select ai wieviele mitarbeiter arbeiten pro abteilung;

-- set context/profile for following statements
EXEC DBMS_CLOUD_AI.set_profile('COHERE');
EXEC DBMS_CLOUD_AI.set_profile('OPENAI');

-- SELECT AI <action> 
-- runsql | showsql | narrate | chat

select ai wieviele mitarbeiter arbeiten pro abteilung;
select ai showsql wieviele mitarbeiter arbeiten pro abteilung;

-- typo
select ai showsql how many employees ae working per derpartment;

select ai showsql welcher beruf verdient im Schnitt am wenigsten;
select ai welcher beruf verdient im Schnitt am wenigsten;

select ai showsql which manager has the most people;


-- create other datamodel with same content, set this as profile and use same SELECT AI statments
CREATE TABLE scott.leute as 
select d.dname as abteilung,
       d.loc   as ort,
       e.empno as ma_nr,
       e.ename as name,
       e.job   as beruf,
       e.sal   as gehalt
  from scott.emp e, scott.dept d
where e.deptno =d.deptno;

select * from scott.leute;

BEGIN
  DBMS_CLOUD_AI.create_profile(
      'COHERE_2',
      '{"provider"       : "cohere",
        "credential_name": "COHERE_CRED",
        "object_list"    : [{"owner": "SCOTT", "name": "LEUTE"}]
       }');
end;
/

exec dbms_cloud_ai.set_profile('COHERE_2');

select ai wieviele mitarbeiter arbeiten pro abteilung;
select ai showsql wieviele mitarbeiter arbeiten pro abteilung;



