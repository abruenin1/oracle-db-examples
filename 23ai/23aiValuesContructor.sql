--------------------------------------
--- Table Values Constructor (ISO) ---
--------------------------------------

SELECT * FROM (VALUES
                  (1,'a','Text a'),
                  (2,'b','Text b'),
                  (3,'c','Text c'),
                  (4,'d','Text d')
               ) virtual_tab (id, letter, description);    
          
              
WITH virtual_tab (id, letter, description) AS
     (VALUES
          (1,'a','Text a'),
          (2,'b','Text b'),
          (3,'c','Text c'),
          (4,'d','Text d')
     )
SELECT * FROM virtual_tab;     

CREATE TABLE emp
       (empno 	 NUMBER(4) NOT NULL,
	    ename 	 VARCHAR2(10),
	    job  	 VARCHAR2(9),
	    mgr  	 NUMBER(4),
	    hiredate DATE,
	    sal    	 NUMBER(7,2),
	    comm 	 NUMBER(7,2),
	    deptno 	 NUMBER(2)
       );
       
CREATE TABLE dept
       (deptno 	NUMBER(2) NOT NULL,
	    dname 	VARCHAR2(14),
	    loc 	VARCHAR2(13) 
       );

INSERT INTO emp (empno,ename,job,mgr,hiredate,sal,comm,deptno) VALUES 
	(7369,'SMITH','CLERK',7902,to_date('17-12-80','DD-MM-YY'),800,NULL,20),
   (7499,'ALLEN','SALESMAN',7698,to_date('20-02-81','DD-MM-YY'),1600,300,30),
   (7521,'WARD','SALESMAN',7698,to_date('22-02-81','DD-MM-YY'),1250,500,30),
	(7566,'JONES','MANAGER',7839,to_date('02-04-81','DD-MM-YY'),2975,NULL,20),
	(7654,'MARTIN','SALESMAN',7698,to_date('28-09-81','DD-MM-YY'),1250,1400,30),
	(7698,'BLAKE','MANAGER',7839,to_date('01-05-81','DD-MM-YY'),2850,NULL,30),
	(7782,'CLARK','MANAGER',7839,to_date('09-06-81','DD-MM-YY'),2450,NULL,10),
	(7788,'SCOTT','ANALYST',7566,SYSDATE-85,3000,NULL,20),
	(7839,'KING','PRESIDENT',NULL,to_date('17-11-81','DD-MM-YY'),5000,NULL,10),
	(7844,'TURNER','SALESMAN',7698,to_date('08-09-81','DD-MM-YY'),1500,0,30),
	(7876,'ADAMS','CLERK',7788,SYSDATE-51,1100,NULL,20),
	(7900,'JAMES','CLERK',7698,to_date('03-12-81','DD-MM-YY'),950,NULL,30),
	(7902,'FORD','ANALYST',7566,to_date('03-12-81','DD-MM-YY'),3000,NULL,20),
	(7934,'MILLER','CLERK',7782,to_date('23-01-82','DD-MM-YY'),1300,NULL,10);
    

INSERT INTO dept (deptno, dname, loc) VALUES 
   (10,'ACCOUNTING','NEW YORK'),
   (20,'RESEARCH','DALLAS'),
   (30,'SALES','CHICAGO'),
   (40,'OPERATIONS','BOSTON');

COMMIT;


-- flexible to use
MERGE INTO emp e
   USING ( VALUES
            ( 7934, 'MILLER', 1),
            ( 7935, 'MILES', 2)
         ) src_tab (empno, ename, sal)
      ON (src_tab.empno = e.empno)
   WHEN MATCHED THEN
      UPDATE SET e.ename = src_tab.ename,
                 e.sal   = src_tab.sal
   WHEN NOT MATCHED THEN
      INSERT (e.empno, e.ename, e.sal)
      VALUES (src_tab.empno, src_tab.ename, src_tab.sal);

SELECT * FROM emp;

ROLLBACK;


