---------------------
-- Top-N/per group --
---------------------

-- Top 3 Employees with highest salary per department

-- pre 23ai: subquery needed ---

SELECT *
FROM
(
  SELECT
    department_id, first_name, last_name, salary,
    ROW_NUMBER() OVER (PARTITION BY department_id ORDER BY salary desc) rn
  FROM employees
)
WHERE rn <= 3
ORDER BY department_id, salary DESC, last_name;

-- 23ai new syntax

SELECT   department_id, first_name, last_name, salary
FROM     EMPLOYEES
ORDER BY department_id, salary DESC
FETCH FIRST 
  9999999 PARTITION BY department_id, 
  3 ROWS ONLY;


