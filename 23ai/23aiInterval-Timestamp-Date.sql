---------------------------------------------------
--- FLOOR & CEIL for DATE, TIMESTAMP & INTERVAL ---
---------------------------------------------------

SELECT sysdate;
ALTER SESSION SET nls_date_format='DD.MM.YYYY HH24:MI:SS';
ALTER SESSION SET nls_timestamp_format='DD.MM.YYYY HH24:MI:SS';

WITH mydates (what,mydate) AS
     (VALUES
          ('sysdate',sysdate),
          ('morning',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 11:11:11' , 'DD.MM.YYYY HH24:MI:SS' )),
          ('grandpa lunchtime',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 12:00:00' , 'DD.MM.YYYY HH24:MI:SS' )),
          ('afternoon',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 15:55:55' , 'DD.MM.YYYY HH24:MI:SS' ))
     )
SELECT what, mydate, trunc(mydate), round(mydate), floor(mydate), ceil(mydate) FROM mydates; 

-- also with HH24
WITH mydates (what,mydate) AS
     (VALUES
          ('sysdate',sysdate),
          ('morning',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 11:11:11' , 'DD.MM.YYYY HH24:MI:SS' )),
          ('grandpa lunchtime',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 12:00:00' , 'DD.MM.YYYY HH24:MI:SS' )),
          ('afternoon',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 15:55:55' , 'DD.MM.YYYY HH24:MI:SS' ))
     )
SELECT what, mydate, trunc(mydate,'HH24'), round(mydate,'HH24'), floor(mydate,'HH24'), ceil(mydate,'HH24') FROM mydates; 

-- and with interval
WITH mytimes (what,mytime) AS
     (VALUES
          ('    100m Worldrecord       ',to_dsinterval('0 00:00:09.58')),
          (' 10.000m Worldrecord       ',to_dsinterval('0 00:26:11')),
          ('Marathon Worldrecord       ',to_dsinterval('0 02:21:09')),
          ('Tortour de Ruhr 100mi, 2022',to_dsinterval('0 16:48:17'))
     )
SELECT what, mytime, trunc(mytime), round(mytime), floor(mytime), ceil(mytime) FROM mytimes; 

-- also with 24hrs
WITH mytimes (what,mytime) AS
     (VALUES
          ('    100m Worldrecord       ',to_dsinterval('0 00:00:09.58')),
          (' 10.000m Worldrecord       ',to_dsinterval('0 00:26:11')),
          ('Marathon Worldrecord       ',to_dsinterval('0 02:21:09')),
          ('Tortour de Ruhr 100mi, 2022',to_dsinterval('0 16:48:17'))
     )
SELECT what, mytime, trunc(mytime,'HH24'), round(mytime,'HH24'), floor(mytime,'HH24'), ceil(mytime,'HH24') FROM mytimes; 


-- Week starts Monday
ALTER SESSION SET NLS_TERRITORY=Germany;
WITH mysysdate as (
    select 
        floor(sysdate,'d') as floordate,
        trunc(sysdate,'d') as truncdate
    )
select floordate, to_char(floordate, 'DY'), truncdate, to_char(truncdate, 'DY')  
from mysysdate;


-------------------------------------
--- Aggregation of INTERVAL Types ---
-------------------------------------

CREATE TABLE Processes
  (	id        NUMBER GENERATED ALWAYS AS IDENTITY,
    ptitle    VARCHAR2(30),
    pstart    TIMESTAMP,
    pend      TIMESTAMP,
    pduration INTERVAL day to second GENERATED ALWAYS AS (pend - pstart) VIRTUAL
   );
   
INSERT INTO processes (ptitle,pstart,pend) VALUES
    ('Process A',trunc(sysdate)+540/1440,trunc(sysdate)+1080/1440),
    ('Process B',trunc(sysdate)+640/1440,trunc(sysdate)+1000/1440),
    ('Process C',trunc(sysdate)+800/1440,trunc(sysdate)+910/1440),
    ('Process D',trunc(sysdate)+800/1440,trunc(sysdate)+1200/1440),
    ('Process E',trunc(sysdate)+840/1440,trunc(sysdate)+1080/1440);
    
SELECT * FROM processes;

SELECT sum(pduration), avg(pduration) FROM processes;


