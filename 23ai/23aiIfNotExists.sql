---------------------------
--- IF [NOT] EXISTS DDL ---
---------------------------


CREATE TABLE    mytable_a (id number, mytext varchar2(40));
CREATE SEQUENCE myseq_a;
CREATE VIEW     myview_a AS SELECT id, mytext FROM mytable_a;
CREATE FUNCTION myfunction_a RETURN NUMBER IS
                   v_count NUMBER;
                BEGIN
                   SELECT count(*) INTO v_count FROM myview_a;
                   RETURN v_count;
                END;
/


DROP TABLE    mytable_a;
DROP SEQUENCE myseq_a;
DROP VIEW     myview_a;
DROP FUNCTION myfunction_a;


CREATE TABLE    IF NOT EXISTS mytable_a (id number, mytext varchar2(40));
CREATE SEQUENCE IF NOT EXISTS myseq_a;
CREATE VIEW     IF NOT EXISTS myview_a AS SELECT id, mytext FROM mytable_a;
CREATE FUNCTION IF NOT EXISTS myfunction_a RETURN NUMBER IS
   v_count NUMBER;
BEGIN
   SELECT count(*) INTO v_count FROM myview_a;
   RETURN v_count;
END;
/

-- CREATE OR REPLACE and IF NOT EXISTS not allowed in same statement
CREATE OR REPLACE FUNCTION IF NOT EXISTS myfunction_a RETURN NUMBER IS
BEGIN
   RETURN null;
END;
/

-- CAUTION: if table exists, nothing happens and no error message
-- ALTER TABLE IF EXISTS does not check columns 
CREATE TABLE IF NOT EXISTS mytable_a (a NUMBER, b NUMBER, c NUMBER, d NUMBER, e NUMBER);

desc mytable_a

DROP TABLE    IF EXISTS mytable_a;
DROP SEQUENCE IF EXISTS myseq_a;
DROP VIEW     IF EXISTS myview_a;
DROP FUNCTION IF EXISTS myfunction_a;
  
-- CAUTION: typo, but no message  
DROP TABLE IF EXISTS chiesecaaake;



