####################
### SQL Firewall ###
####################
### App User     ###
####################

--- 1 ---

SELECT count(*) FROM scott.one;
SELECT *        FROM scott.two;
INSERT INTO scott.one VALUES (3,'THIEM');
COMMIT;

--- sys 1 --->

--- 2 ---

SELECT count(*) FROM scott.one;
SELECT count(*) FROM scott.two;
SELECT *        FROM scott.two;

--- sys 2 --->

--- 3 ---

SELECT count(*) FROM scott.two;

--- sys 3 --->






