------------------------------
--- Boolean Datatype (ISO) ---
------------------------------


CREATE TABLE boolean_datatype (id          NUMBER GENERATED ALWAYS AS IDENTITY,
                               bool_value1 BOOLEAN,
                               bool_value2 BOOLEAN,
                               bool_input  VARCHAR2(20));
                               
INSERT INTO boolean_datatype (bool_value1, bool_value2, bool_input) 
VALUES
   (true,false    ,'true/false'),
   (TRUE,FALSE    ,'TRUE/FALSE'), 
   (1,0           ,'1/0 as number'),
   ('true','false','true/false as String'),
   ('TRUE','FALSE','TRUE/FALSE as String'),
   ('TRuE','faLSE','TReE/faLSE as String'),
   ('yes' ,'no'   ,'yes/no as String'),
   ('YES' ,'NO'   ,'YES/NO as String'),
   ('on'  ,'off'  ,'on/off as String'),
   ('ON'  ,'OFF'  ,'ON/OFF as String'),
   ('1'   ,'0'    ,'1/0 as String'),
   ('t'   ,'f'    ,'t/f as String'),
   ('T'   ,'F'    ,'T/F as String'),
   ('y'   ,'n'    ,'y/n as String'),
   ('Y'   ,'N'    ,'Y/N as String'),
   (null,null,'null as null');
  

INSERT INTO boolean_datatype (bool_input) VALUES ('"nothing" as null');

INSERT INTO boolean_datatype (bool_value1, bool_value2, bool_input) VALUES ('ja','nein','ja/nein');

INSERT INTO boolean_datatype (bool_value1, bool_value2, bool_input)  
   SELECT to_boolean('true'), to_boolean(0),'explicit conversion'; 
   
COMMIT;
   
--- Display (1 or true) depending from version of tool/driver
SELECT * FROM boolean_datatype;

-- Only in SQL*Plus
COLUMN bool_value1 BOOLEAN yes no
COLUMN bool_value2 BOOLEAN yes no
SELECT * FROM boolean_datatype;

-- But explicit conversion available in Java based tools
SELECT true, to_char(false), to_char(bool_value1), to_number(bool_value2) FROM boolean_datatype;

SELECT * FROM boolean_datatype WHERE bool_value1 IS true;

SELECT * FROM boolean_datatype WHERE bool_value1;

SELECT * FROM boolean_datatype WHERE bool_value1 IS null;

--- in PL/SQL plsql_implicit_conversion_bool=true for overloading conversion functions
set serveroutput on

DECLARE
  v_bool    BOOLEAN := true;
  v_number  NUMBER;
BEGIN
  v_number := v_bool;
  DBMS_OUTPUT.PUT_LINE( 'Die Antwort auf alle Fragen ist: ' || v_number);
End;
/

--- in PL/SQL plsql_implicit_conversion_bool=true for overloading conversion functions
ALTER SESSION SET plsql_implicit_conversion_bool=true;
ALTER SESSION SET plsql_implicit_conversion_bool=false;

