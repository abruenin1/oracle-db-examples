----------------------
--- Developer Role ---
----------------------

-- as sys
-- what's in the connect role
SELECT 'SysPriv' as priv, privilege as priv, null as tab FROM dba_sys_privs  WHERE grantee = 'CONNECT'
  UNION
SELECT 'RolePriv'       , granted_role     , null        FROM dba_role_privs WHERE grantee = 'CONNECT'
  UNION
SELECT 'TabPriv'        , privilege        , table_name  FROM dba_tab_privs  WHERE grantee = 'CONNECT'
ORDER BY 1,2,3;

-- what's in the resource role
SELECT 'SysPriv' as priv, privilege as priv, null as tab FROM dba_sys_privs  WHERE grantee = 'RESOURCE'
  UNION
SELECT 'RolePriv'       , granted_role     , null        FROM dba_role_privs WHERE grantee = 'RESOURCE'
  UNION
SELECT 'TabPriv'        , privilege        , table_name  FROM dba_tab_privs  WHERE grantee = 'RESOURCE'
ORDER BY 1,2,3;

-- what's additionally to those privs in connect & resource in the new Developer Rolee role
(SELECT 'SysPriv' as priv, privilege as priv, null as tab FROM dba_sys_privs  WHERE grantee = 'DB_DEVELOPER_ROLE'
  UNION
SELECT 'RolePriv'       , granted_role     , null        FROM dba_role_privs WHERE grantee = 'DB_DEVELOPER_ROLE'
  UNION
SELECT 'TabPriv'        , privilege        , table_name  FROM dba_tab_privs  WHERE grantee = 'DB_DEVELOPER_ROLE')
  MINUS 
(SELECT 'SysPriv' as priv, privilege as priv, null as tab FROM dba_sys_privs  WHERE grantee = 'RESOURCE'
  UNION
SELECT 'RolePriv'       , granted_role     , null        FROM dba_role_privs WHERE grantee = 'RESOURCE'
  UNION
SELECT 'TabPriv'        , privilege        , table_name  FROM dba_tab_privs  WHERE grantee = 'RESOURCE')
 MINUS
(SELECT 'SysPriv' as priv, privilege as priv, null as tab FROM dba_sys_privs  WHERE grantee = 'CONNECT'
  UNION
SELECT 'RolePriv'       , granted_role     , null        FROM dba_role_privs WHERE grantee = 'CONNECT'
  UNION
SELECT 'TabPriv'        , privilege        , table_name  FROM dba_tab_privs  WHERE grantee = 'CONNECT')
ORDER BY 1,2,3;


