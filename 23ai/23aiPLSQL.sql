---------------------------------------------
--- SQL UPDATE RETURN Clause Enhancements ---
---------------------------------------------

set serveroutput on

-- RETURNING old and new
DECLARE
   p_empno  emp.empno%TYPE := 7499;
   v_sal_o  emp.sal%TYPE;
   v_sal_n  emp.sal%TYPE;
   v_comm_o emp.comm%TYPE;
   v_comm_n emp.comm%TYPE;
   v_ename  emp.ename%TYPE;
BEGIN
  UPDATE emp
     SET sal  = (sal * 1.05) + 100,
         comm = comm + 10
   WHERE empno = p_empno
   RETURNING  OLD sal, OLD comm, NEW sal, NEW comm, OLD ename
        INTO  v_sal_o, v_comm_o, v_sal_n, v_comm_n, v_ename;
        
   DBMS_OUTPUT.PUT_LINE('SAL changed from '||to_char(v_sal_o) ||' to '||to_char(v_sal_n)||' for '||initcap(v_ename) );     
   DBMS_OUTPUT.PUT_LINE('COMM changed from '||nvl(to_char(v_comm_o),'nothing')||' to '||nvl(to_char(v_comm_n),'nothing')||' for '||initcap(v_ename) );
END;
/

-- bulk collect into table
DECLARE
   TYPE amounts IS TABLE OF emp.sal%TYPE;
   TYPE enames  IS TABLE OF emp.ename%TYPE;
   v_sal_o  amounts;
   v_sal_n  amounts;
   v_comm_o amounts;
   v_comm_n amounts;
   v_ename  enames;
BEGIN
  UPDATE emp
     SET sal  = (sal * 1.05) + 100,
         comm = comm + 10
   WHERE deptno = 10    
   RETURNING          OLD sal, OLD comm, NEW sal, NEW comm, OLD ename
   BULK COLLECT INTO  v_sal_o, v_comm_o, v_sal_n, v_comm_n, v_ename;
        
  FOR emps IN 1..v_sal_o.COUNT LOOP        
     DBMS_OUTPUT.PUT_LINE('---- change '||to_char(emps));
     DBMS_OUTPUT.PUT_LINE('SAL changed from '||to_char(v_sal_o(emps)) ||' to '||to_char(v_sal_n(emps))||' for '||initcap(v_ename(emps)) );     
     DBMS_OUTPUT.PUT_LINE('COMM changed from '||nvl(to_char(v_comm_o(emps)),'nothing')||' to '||nvl(to_char(v_comm_n(emps)),'nothing')||' for '||initcap(v_ename(emps)) );
  END LOOP;   
END;
/

--- available for INSERT & UPDATE with NULLS for OLD (insert) respectively NEW (delete) values 


--------------------------------------
--- CASE [EXPRESSION] enhancements ---
--------------------------------------
 
CREATE TABLE cpu_load (id              NUMBER GENERATED ALWAYS AS IDENTITY,
                       percentage_load NUMBER);

INSERT INTO cpu_load (percentage_load) VALUES
  (-8),(0),(15),(46),(60),(61),(76),(80),(88),(100),(107);

SET SERVEROUTPUT ON

-- works with older versions
DECLARE
  v_status VARCHAR2(20);
BEGIN
  FOR c IN (SELECT percentage_load FROM cpu_load) LOOP
    v_status :=
    CASE 
      WHEN c.percentage_load  < 0   THEN 'impossible'
      WHEN c.percentage_load  = 60  THEN 'great'
      WHEN c.percentage_load <= 70  THEN 'ok'
      WHEN c.percentage_load  = 80  THEN 'great'
      WHEN c.percentage_load <= 85  THEN 'observe'
      WHEN c.percentage_load <= 100 THEN 'check'
      ELSE                               'impossible'
    END;
    DBMS_OUTPUT.PUT_LINE(c.percentage_load || ' : ' || v_status);
  END LOOP;
END;
/

-- 23ai: multiple choices, no left operand (dangling predicate)
DECLARE
  v_status VARCHAR2(20);
BEGIN
  FOR c IN (SELECT percentage_load FROM CPU_LOAD) LOOP
    v_status :=
    CASE c.percentage_load
      WHEN  <0, >100  THEN 'impossible'
      WHEN 60, 80     THEN 'great'
      WHEN <= 70      THEN 'ok'
      WHEN <= 85      THEN 'observe'
      ELSE                 'check'
    END;
    DBMS_OUTPUT.PUT_LINE(c.percentage_load || ' : ' || v_status);
  END LOOP;
END;
/

