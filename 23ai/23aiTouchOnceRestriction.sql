------------------------------------------------------------
--- Removal of Touch-Once Restriction after Parallel DML ---
------------------------------------------------------------

SELECT * FROM v$version;

CREATE TABLE otr AS SELECT * FROM all_objects;

SELECT count(*) FROM otr;

ALTER SESSION enable parallel dml;

INSERT /*+ parallel(otr 8) */ INTO otr
   SELECT * FROM otr;
   
-- Would throw an ORA-12838 on older versions
-- Need to commit or rollback before referencing a table modified by the parallel DML
-- But works with 23c
SELECT count(*) FROM otr;

ROLLBACK;



