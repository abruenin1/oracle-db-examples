---------------------------------------
--- GROUP BY & HAVING using Aliases ---
---------------------------------------

SELECT initcap(dname) || ' - (' || initcap(loc) || ')' AS department, 
       count(*)                                        AS num_of_employees, 
       sum(sal+nvl(comm,0))                            AS sum_income
FROM   dept, emp
WHERE  emp.deptno = dept.deptno
GROUP BY initcap(dname) || ' - (' || initcap(loc) || ')'
HAVING count(*) > 3                   
   AND sum(sal+nvl(comm,0)) < 11000;


SELECT initcap(dname) || ' - (' || initcap(loc) || ')' AS department, 
       count(*)                                        AS num_of_employees, 
       sum(sal+nvl(comm,0))                            AS sum_income
FROM   dept, emp
WHERE  emp.deptno = dept.deptno
GROUP BY department
HAVING num_of_employees > 3                   
   AND sum_income < 11000;


SELECT initcap(dname) || ' - (' || initcap(loc) || ')' AS department, 
       count(*)                                        AS num_of_employees, 
       sum(sal+nvl(comm,0))                            AS sum_income
FROM   dept, emp
WHERE  emp.deptno = dept.deptno
GROUP BY 1                  -- reference by number allowed depending parameter
HAVING num_of_employees > 3                   
   AND sum_income < 11000;

ALTER SESSION SET group_by_position_enabled=true;
ALTER SESSION SET group_by_position_enabled=false;


