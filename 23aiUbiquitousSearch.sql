---------------------------------------------------------------------------
-- 23c Ubiquitous Database Search example
-- 
-- https://docs.oracle.com/en/database/oracle/oracle-database/23/ccapp/using-ubiquitous-database-search.html
--
-- use HR example Schema https://github.com/oracle-samples/db-sample-schemas
-- For easier setup use https://github.com/oracle-samples/db-sample-schemas
--
---------------------------------------------------------------------------

-- CREATE TABLE employees AS SELECT * FROM hr.employees;
-- CREATE TABLE departments AS SELECT * FROM hr.departments;

-- OPTIONAL: easy creation of search indexes
/*
create search index dep_ctx_idx on DEPARTMENTS(department_name)
    parameters('MAINTENANCE AUTO');
create search index emp_ctx_idx on employees(last_name);
select * from departments where contains( department_name, 'Shipping' ) > 0;
select * from v$text_waiting_events;
select idx_name, idx_maintenance_type from ctx_user_indexes;
*/

-- create multi table search index;
exec dbms_search.create_index('EMPDEPT');

SELECT table_name FROM user_tables ORDER BY 1 DESC;

DESC empdept;

SELECT * FROM user_objects where object_name = 'EMPDEPT';

SELECT * FROM empdept;

-- This fails intentionally
exec dbms_search.add_source('EMPDEPT', 'EMPLOYEES');

-- add primary key and retry
ALTER TABLE employees ADD CONSTRAINT employees_pk PRIMARY KEY(employee_id);
exec dbms_search.add_source('EMPDEPT', 'EMPLOYEES');

SELECT * FROM empdept;

-- see, what is really indexed
SELECT dbms_search.get_document(INDEX_NAME=>'EMPDEPT', DOCUMENT_METADATA=>METADATA) searchdoc 
FROM   empdept;

SELECT metadata FROM empdept 
WHERE  contains(data,'king')>0;
     
SELECT * FROM employees WHERE employee_id IN( 100, 156 );

-- add data sources
ALTER TABLE departments ADD CONSTRAINT department_pk PRIMARY KEY(department_id);

exec dbms_search.add_source('EMPDEPT', 'DEPARTMENTS');

SELECT distinct(source) FROM empdept;

INSERT INTO departments (
    department_id, department_name
) Values (
    999, 'Kings Landing'
);

SELECT  metadata 
FROM    empdept
WHERE   contains(data, 'king%') > 0;

-- commit first for text index sync and retry
COMMIT;

-- finds results from all tables
SELECT  metadata 
FROM    empdept
WHERE   contains(data, 'king%') > 0;

SELECT dbms_search.get_document(INDEX_NAME=>'EMPDEPT', DOCUMENT_METADATA=>METADATA) searchdoc 
FROM   empdept
WHERE   contains(data, 'king%') > 0;

-- query only the departments table
SELECT  metadata 
FROM    empdept
WHERE   contains(data, 'king%') > 0
AND     source = 'DEPARTMENTS';

SELECT  metadata 
FROM    empdept
WHERE   json_textcontains(data, '$.SCOTT.DEPARTMENTS', 'king%');


-- clean up
exec dbms_search.drop_index('EMPDEPT');



