/*
   Required grants for EXPLAIN PLAN
   https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/EXPLAIN-PLAN.html#GUID-FD540872-4ED3-4936-96A2-362539931BA0
*/

-- run as sys
grant select on V_$SQL_WORKAREA to dev;
grant select on V_$SQL_PLAN to dev;
grant select on V_$SQL_PLAN_STATISTICS to dev;
grant select on V_$SQL_PLAN_STATISTICS_ALL to dev;
grant select on V_$SQL_PLAN_MONITOR to dev;
GRANT SELECT ON V_$MYSTAT to dev;
GRANT SELECT ON V_$SQL to dev;
GRANT SELECT ON V_$SESSION to dev;
grant select on V_$SQL_PLAN_STATISTICS_ALL to dev;

-- run as user
@utlxplan.sql