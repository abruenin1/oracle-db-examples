-------------------------------
--- Oracle 21c SQL Examples ---
-------------------------------
 
SET SERVEROUTPUT ON

--- 21c Qualified Expressions
DECLARE
  TYPE chartab_t IS TABLE OF VARCHAR(16) INDEX BY PLS_INTEGER;
  chartab chartab_t;
BEGIN
  -- old syntax
  chartab(1) := '12c';
  chartab(2) := 'old';
  chartab(3) := 'syntax';

  -- Better with 18c Qualified Expressions
  chartab := chartab_t( 1 => '18c', 2 => 'newer', 3 => 'syntax');

  -- 21c - Qualified expressions
  chartab := chartab_t('21c', 'lastest', 'syntax');

  DBMS_OUTPUT.PUT_LINE(  '===> ' || chartab(1) || ' ' || chartab(2)  || ' ' || chartab(3) );
END;
/

--- FOR LOOP enhancements - 21c Basic Iterators
BEGIN
    FOR i IN 1 ..5, 100..105, 200..205 LOOP
        DBMS_OUTPUT.PUT_LINE(i);
    END LOOP;
END;
/
 
BEGIN
    FOR i IN 1 .. 20 BY 3 LOOP
        DBMS_OUTPUT.PUT_LINE(i);
    END LOOP;
END;
/

-- does not work AS expected ... rounded to 1..6 BY 1
BEGIN
    FOR i IN 0.5 .. 5.5 BY 0.5 LOOP
        DBMS_OUTPUT.PUT_LINE(i);
    END LOOP;
END;
/

-- ... but we can now set the TYPE FOR the iterator
BEGIN
    FOR i number(2,1) IN 0.5 .. 5.5 BY 0.5 LOOP
        DBMS_OUTPUT.PUT_LINE(i);
    END LOOP;
END;
/

-- REVERSE
BEGIN
  FOR i IN REVERSE 1..6 LOOP
    DBMS_OUTPUT.PUT_LINE(i);
  END LOOP;
END;
/

-- Mix AS you like
BEGIN
  FOR i IN 1..3, REVERSE 4..6, 14..16 LOOP
    DBMS_OUTPUT.PUT_LINE(i);
  END LOOP;
END;
/

-- New table setup and INDIDES operator
DECLARE
    TYPE num_list IS TABLE OF INT INDEX BY PLS_INTEGER;
    s1   num_list := num_list(18, 20, 2, 0, 4, 27, 30, 33, 35, 36); -- NEW 21c
BEGIN
    FOR idx IN INDICES OF s1 LOOP
        DBMS_OUTPUT.PUT_LINE( idx || ': ' || s1(idx) );
    END LOOP;
END;
/

-- but INDICES also works with sparse arrays
DECLARE
  TYPE inttab_t IS TABLE OF PLS_INTEGER INDEX BY PLS_INTEGER;
  sparsetab inttab_t := inttab_t(1 => 18, 3 => 20, 5 => 20, 7 => 0);
BEGIN
  FOR i IN INDICES OF sparsetab LOOP
    DBMS_OUTPUT.PUT_LINE(i || ': ' || sparsetab(i) );
  END LOOP;
END;
/
  
-- or use VALUES operator
DECLARE
    TYPE num_list IS TABLE OF INT INDEX BY PLS_INTEGER;
    s1   num_list := num_list(18, 20, 2, 0, 24, 27, 30, 33, 35, 36);
BEGIN
    FOR val IN VALUES OF s1 LOOP
        DBMS_OUTPUT.PUT_LINE( val );
    END LOOP;
END;
/
  
-- or both with PAIRS operator
DECLARE
    TYPE num_list IS TABLE OF INT INDEX BY PLS_INTEGER;
    s1   num_list := num_list(18, 20, 2, 0, 24, 27, 30, 33, 35, 36); -- NEW
BEGIN
    FOR idx, val IN PAIRS OF s1 LOOP
        DBMS_OUTPUT.PUT_LINE( idx || ': ' || val );
    END LOOP;
END;
/

-- Calculate with the index
BEGIN
    FOR power2 IN 1, REPEAT power2*2 WHILE power2 <= 256 LOOP
            DBMS_OUTPUT.PUT_LINE(power2 || ' ' );
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('');
END;
/

  
-- or go completely nuts ... if you can read it later
BEGIN
    FOR i IN 1 .. 10 WHILE i<5
        ,6..15 BY trunc(i/4)
        ,i..i+10 WHEN mod(i,3) = 0
    LOOP
        DBMS_OUTPUT.PUT_LINE(i);
    END LOOP;
END;
/

-- 21c Extended Iterators
DECLARE
  TYPE inttab_t IS TABLE OF PLS_INTEGER INDEX BY PLS_INTEGER;
  inttab  inttab_t;
BEGIN
  inttab := inttab_t(FOR i IN 1..6 => i*10); 

  FOR int,val IN PAIRS OF inttab LOOP
    DBMS_OUTPUT.PUT_LINE( int || ': ' || val);
  END LOOP;
END;
/
 
-- more complex example
DECLARE
  TYPE inttab_t IS TABLE OF PLS_INTEGER INDEX BY PLS_INTEGER;
  inttab  inttab_t;
BEGIN
  inttab := inttab_t(
    FOR i IN 0..100 BY 5 
        WHILE i<30 
        WHEN mod(i,2) != 0 
        => i*10
    ); 

  FOR int,val IN PAIRS OF inttab LOOP
    DBMS_OUTPUT.PUT_LINE( int || ': ' || val);
  END LOOP;
END;
/

-- Use extended iterator with cursor
DECLARE
  TYPE dept_t IS TABLE OF hr.departments%rowTYPE INDEX BY PLS_INTEGER;
  dept_tab  dept_t;
BEGIN
  dept_tab := dept_t(
    FOR i IN (select department_id, department_name, manager_id, location_id from hr.departments)
        WHILE (i.department_id<70)
        WHEN mod(i.department_id,20)=0
        INDEX i.department_id => i
    ); 

  FOR idx,row IN PAIRS OF dept_tab LOOP
    DBMS_OUTPUT.PUT_LINE( idx || ': ' || row.department_name );
  END LOOP;
END;
/


--- OPTIONAL ---

--- 
--- Enhanced SQL Set Operators
--- More ANSI standard compatibility
--- 

/*  The SQL set operators now support all keywords AS defined IN ANSI SQL. 
    The new operator EXCEPT [ALL] is functionally equivalent to MINUS [ALL]. 
    The operators MINUS and INTERSECT now support the keyword ALL.
*/

CREATE TABLE dogs AS 
    SELECT * FROM (VALUES
        (1, 1, 'dog'),
        (2, 1, 'dog'),
        (2, 1, 'dog')
    ) vtab (id, type, name);

CREATE TABLE cats AS 
    SELECT * FROM (VALUES
        (4, 1, 'cat'),
        (5, 1, 'cat'),
        (5, 1, 'cat')
    ) vtab (id, type, name); 

-- UNION vs (ALL) ... quite some time available
SELECT * FROM dogs
    UNION ALL
SELECT * FROM cats;

SELECT * FROM dogs
    UNION 
SELECT * FROM cats;

CREATE OR REPLACE VIEW pets AS  
    SELECT * FROM dogs
        UNION ALL
    SELECT * FROM cats;

SELECT * FROM pets;

-- 21c EXCEPT [ALL] is functionally equivalent to MINUS [ALL] FOR ANSI compatibility
SELECT * FROM pets 
    EXCEPT
SELECT * FROM dogs;
    
SELECT * FROM pets 
    MINUS
SELECT * FROM dogs;
    
SELECT * FROM pets 
    EXCEPT ALL
SELECT * FROM dogs;
    
SELECT * FROM pets 
    MINUS ALL
SELECT * FROM dogs;

-- 21c The operators MINUS and INTERSECT now support the keyword ALL. 
SELECT * FROM pets 
    INTERSECT
SELECT * FROM dogs;
    
SELECT * FROM pets 
    INTERSECT ALL
SELECT * FROM dogs;

-- ANY_VALUE
-- Use ANY_VALUE to optimize a query that has a GROUP BY clause,
-- returns a value more quickly than MIN or MAX IN a GROUP BY query. 

SELECT d.department_id, any_value(d.department_name), count(e.employee_id)
  FROM departments d
  LEFT OUTER JOIN employees e ON e.department_id = d.department_id
  GROUP BY d.department_id;


/*
    Bitwise Aggregation Functions
*/

SELECT BIT_AND_AGG(bits) band, BIT_OR_AGG(bits) bor, BIT_XOR_AGG(bits) bxor
FROM (VALUES
        (7,'0111', bin_to_num(0,1,1,1)),
        (5,'0101', bin_to_num(0,1,0,1)),
        (8,'1000', bin_to_num(1,0,0,0))
    ) vtab (num, bin, bits);


/*
    Oracle Blockchain Table
*/

CREATE BLOCKCHAIN TABLE blockchain_1(
   id          NUMBER GENERATED ALWAYS AS IDENTITY,
   insertdate  DATE DEFAULT sysdate,
   oraversion  VARCHAR2(256)
)
NO DROP UNTIL 1 DAYS IDLE
NO DELETE UNTIL 16 DAYS AFTER INSERT
HASHING USING "SHA2_512" VERSION "V1";

desc blockchain_1

-- some hidden columns
SELECT * FROM user_tab_cols WHERE table_name = 'blockchain_1';   

-- new DD views
SELECT * FROM user_blockchain_tables;

INSERT INTO blockchain_1(oraversion) VALUES
   ('8i'), ('9i'), ('10g'), ('11g'), ('12c'), ('18c'), ('19c'), ('21c'), ('23ai' );
Commit;

SELECT * FROM blockchain_1;

-- no modifications allowed
DELETE FROM blockchain_1 WHERE id=1;

TRUNCATE TABLE blockchain_1;

UPDATE blockchain_1 SET oraversion = '12.2.0.2' WHERE oraversion = '18c';

-- retention period cannot be reduced
ALTER TABLE blockchain_1 NO DROP UNTIL 0 days IDLE;

-- longer retention period is OK
ALTER TABLE blockchain_1 NO DROP UNTIL 2 days IDLE;

-- but you cannot go back
ALTER TABLE blockchain_1 NO DROP UNTIL 1 days IDLE;
   
-- modify allowed, because shorter than longest data is forbidden anyways
ALTER TABLE blockchain_1 MODIFY (oraversion VARCHAR2(16));

-- but not adding or dropping columns
ALTER TABLE blockchain_1 ADD (extra VARCHAR2(16) );
ALTER TABLE blockchain_1 DROP (oraversion);
    
-- Verify Blockchain Integrity    
DECLARE
  verified  NUMBER;
BEGIN
   DBMS_BLOCKCHAIN_TABLE.VERIFY_ROWS(
      schema_name             => 'SCOTT',
      table_name              => 'BLOCKCHAIN_1',
      number_of_rows_verified => verified);
   DBMS_OUTPUT.PUT_LINE( 'Verified Rows: ' || verified);
END;
/    

-- Purge expired rows
DECLARE
  rows   NUMBER;
BEGIN
   DBMS_BLOCKCHAIN_TABLE.DELETE_EXPIRED_ROWS(
      schema_name             => 'SCOTT',
      table_name              => 'BLOCKCHAIN_1',
      before_timestamp        => systimestamp-30,
      number_of_rows_deleted  => rows);
   DBMS_OUTPUT.PUT_LINE('Rows deleted: ' || rows);
END;
/


DROP TABLE IF EXISTS blockchain_1;

