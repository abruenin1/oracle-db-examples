/*
   21c Oracle Text Example
*/


--- run as sys
grant create any directory to scott;

--- run as user
exec ctx_ddl.drop_preference('MYDS');
drop index TextDirDS_idx;
drop table if exists TextDirDS;

create table TextDirDS (
    id number primary key, 
    text varchar2(2000)
);

insert into TextDirDS values 
    (1, '21cText.sql'),
    (2, '21cSQL-PLSQL.sql');

commit;

select * from TextDirDS;

/*  New DIRECTORY_DATASTORE
    FILE_DATASTORE Deprecated for security reasons
    ctx_ddl.create_preference('mypref', 'FILE_DATASTORE');
    ctx_ddl.set_attribute('mypref', 'PATH', '/home/oracle/git/oracle-db-examples/23c')
    No file access without DIRECTORY anymore */

create or replace directory mydir as '/home/oracle/git/oracle-db-examples/21c';

exec ctx_ddl.create_preference('MYDS','DIRECTORY_DATASTORE');
exec ctx_ddl.set_attribute('MYDS','DIRECTORY','mydir');

create index TextDirDS_idx on TextDirDS(text) indextype is ctxsys.context
    parameters ('datastore MYDS');


select * from TextDirDS where contains(text, 'TextDirDS')>0;
select * from TextDirDS where contains(text, 'dogs')>0;


/*  --- Oracle Text NETWORK_DATASTORE ---
    New NETWORK_DATASTORE Data Store Type for Oracle Text
    URL_DATASTORE is deprecated for security reasons and HTTPS 
    Run 12cNetwork-ACLS.sql first */

-- check access to target host
select * from table( dbms_network_acl_utility.domains('workshop.local') );

exec ctx_ddl.drop_preference('NETWORK_PREF');
drop index website_idx;
drop table if exists urltable;

create table urltable as 
    select * from (values
        (1, 'Menschen Leben Tanzen Welt',
         'http://localhost:8080/Menschen'),
        (2, 'Hommingberger Gepardenforelle', 
         'http://localhost:8080/Gepardenforelle')
    ) vtab (id, title, website);

commit;

select * from urltable;

begin
    ctx_ddl.create_preference('NETWORK_PREF','NETWORK_DATASTORE');
    ctx_ddl.set_attribute('NETWORK_PREF','NO_PROXY','localhost.local');
    ctx_ddl.set_attribute('NETWORK_PREF','TIMEOUT','900');
end;
/

create index website_idx on urltable(website)
    indextype is ctxsys.context
    parameters ('datastore NETWORK_PREF filter ctxsys.null_filter section group ctxsys.html_section_group');
    
select * from CTX_USER_INDEX_ERRORS;

exec ctx_ddl.sync_index('website_idx');

select * from urltable where contains(website, 'Tanzen')>0;
select * from urltable where contains(website, 'Gepard')>0;


/* In-Memory Full Text Columns
   You can apply the INMEMORY TEXT clause to non-scalar columns in an In-Memory table. 
   This clause enables fast In-Memory searching of text, XML, or JSON documents using the CONTAINS() orJSON_TEXTCONTAINS () operators .
*/

drop table if exists klugesprueche;
create table klugesprueche (
   id    number generated always as identity,
   searchtext  varchar2(1024)
) INMEMORY TEXT(searchtext);

insert into klugesprueche(searchtext) values 
   ('Am Ende wird alles gut. Wenn es nicht gut ist, ist es noch nicht das Ende.'),
   ('Gewohnheiten sind zuerst Spinnweben, dann Dr�hte.'),
   ('Ehrlichkeit verschafft dir vielleicht nicht viele Freunde, daf�r aber die Richtigen.'),
   ('Das Vergleichen ist das Ende des Gl�cks und der Anfang der Unzufriedenheit.');
commit;

create index klugesprueche_idx on klugesprueche(searchtext) 
   indextype is ctxsys.context;

select searchtext from klugesprueche where contains(searchtext, 'ende')>0;

--- EOF ---
